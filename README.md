This project is the reference implementation of the protocol defined in the contrib abcl-stepper that allows to visualize the internal states of the stepper process in a different UI.

In this implementation we use HTMX and Websockets to achieve the new UI in a web browser.

The abcl-stepper contrib define the class 'stepper:render-client' and its methods:

'stepper:send-code'

'stepper:clean-connection'

'stepper:connect-to-websocket'

'stepper:read-user-action'

When implemented, these methods allows to connect to an external server in order to render the internal workflow of the stepping process.

This implementation can be seen as a guide to implement new UIs for improve the UX of the abcl-stepper contrib.

The arquitecture is simple:


| ABCL +  ABCL-RENDER-CLIENT | <-> | ABCL + ABCL-STEPPER-RENDER | -  UI actions


In the left side we will have an instance of ABCL that will load the render-client component that will allow to
connect to the render-server side (rigth side, the other component of this visual stepper), with another ABCL instance that
will run a websocket server and a web application. The websocket server will act as a router
between the user interaction on the HTML UI and the step process that will run on the left sending the message from one side
to another.

The right side can be implemented with any other backend language, I just did it with ABCL to show another practical usage
of the implementation and for fun ;)

Dependencies:

ABCL-RENDER-CLIENT depends on html-template(on Quicklisp) & my abcl-websocket-client (https://gitlab.com/cl-projects/abcl-websocket-client)

ABCL-STEPPER-RENDER depends also on html-template & my abcl-websocket-server(https://gitlab.com/cl-projects/abcl-websocket-server)

Make sure you can have all the dependencies and they be load using 'ql:quickload'.

You can also use Qlot to get the dependencies, following the next steps

$ docker run --rm -it -v .:/app fukamachi/qlot install

$ sudo chown -R ${USER}. .qlot/

$ sudo chown -R ${USER}. qlfile.lock

After that you can load the file '.qlot/setup.lisp' when loading the ABCL instance
in order to use the local Quicklisp distributions containing the dependencies code.
Something like:

$ abcl --noinit --load .qlot/setup.lisp


Usage:

$ git clone --recurse-submodules https://gitlab.com/cl-projects/abcl-visual-stepper.git

Assuming you have 'abcl' in your path

$ abcl --load abcl-visual-stepper/abcl-stepper-render/start-server.lisp -- /myws 7000

Open your web browser at http://localhost:7000/main

Open another abcl instance and do:

```
CL-USER> (require :asdf)
NIL
CL-USER> (require :abcl-contrib)
NIL
CL-USER> (require :abcl-stepper)
NIL
CL-USER> (require :abcl-asdf)
NIL
CL-USER> (require :quicklisp-abcl)
NIL
CL-USER> (ql:quickload :abcl-render-client)
To load "abcl-render-client":
  Load 1 ASDF system:
    abcl-render-client
; Loading "abcl-render-client"
.
(:ABCL-RENDER-CLIENT)
CL-USER> (abcl-render-client:visual-step (list (list 3) (list 7))
                                         "/myws" 7000)
```

or another form you want to step in

After that you go again to the browser and perform all the stepping operations from the web application.
